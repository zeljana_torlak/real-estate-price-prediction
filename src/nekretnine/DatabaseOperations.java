package nekretnine;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import nekretnine.entiteti.Nekretnina;

public class DatabaseOperations {

    private static final EntityManager em = Persistence.createEntityManagerFactory("NekretninePU").createEntityManager();

    public static Nekretnina napraviNekretninu(Integer tip, Integer tipPonude, String grad, String deoGrada, BigDecimal kvadratura,
            Integer godinaIzgradnje, BigDecimal povrsinaZemljista, String spratnost, Integer uknjizenost, String tipGrejanja,
            BigDecimal brojSoba, String unutrasnjeProstorije, Integer parking, Integer garaza, Integer lift,
            String infrastruktura, Integer cena, String link) {
        Nekretnina nekretnina = new Nekretnina();
        nekretnina.setTip(tip);
        nekretnina.setTipPonude(tipPonude);
        nekretnina.setGrad(checkLength(grad, 100));
        nekretnina.setDeoGrada(checkLength(deoGrada, 100));
        nekretnina.setKvadratura(kvadratura);
        nekretnina.setGodinaIzgradnje(godinaIzgradnje);
        nekretnina.setPovrsinaZemljista(povrsinaZemljista);
        nekretnina.setSpratnost(checkLength(spratnost, 50));
        nekretnina.setUknjizenost(uknjizenost);
        nekretnina.setTipGrejanja(checkLength(tipGrejanja, 50));
        nekretnina.setBrojSoba(brojSoba);
        nekretnina.setUnutrasnjeProstorije(checkLength(unutrasnjeProstorije, 50));
        nekretnina.setParking(parking);
        nekretnina.setGaraza(garaza);
        nekretnina.setLift(lift);
        nekretnina.setInfrastruktura(checkLength(infrastruktura, 50));
        nekretnina.setCena(cena);
        nekretnina.setLink(checkLengthWithNull(link, 200));

        try {
            em.getTransaction().begin();
            em.persist(nekretnina);
            em.getTransaction().commit();

            return nekretnina;
        } catch (Exception e) {
            return null;
        }
    }

    public static List<Nekretnina> dovatiSveNekretnine() {
        return em.createQuery("SELECT n FROM Nekretnina n ORDER BY n.id ASC", Nekretnina.class).getResultList(); //em.createNamedQuery("Nekretnina.findAll", Nekretnina.class)
    }

    public static List<Nekretnina> dovatiSveStanoveKojiSeProdajuUBeogradu() {
        return em.createQuery("SELECT n FROM Nekretnina n WHERE n.tip = 0 AND n.tipPonude = 0 AND n.grad = 'beograd' ORDER BY n.id ASC", Nekretnina.class).getResultList();
    }

    public static double dohvatiNajvecuKvadraturu() {
        BigDecimal kvadratura = em.createQuery("SELECT MAX(n.kvadratura) FROM Nekretnina n WHERE n.tip = 0 AND n.tipPonude = 0 AND n.grad = 'beograd'", BigDecimal.class).getSingleResult();
        if (kvadratura == null) {
            return 0;
        }
        return kvadratura.doubleValue();
    }

    public static double dohvatiNajvecuGodinuIzgradnje() {
        Integer godinaIzgradnje = em.createQuery("SELECT MAX(n.godinaIzgradnje) FROM Nekretnina n WHERE n.tip = 0 AND n.tipPonude = 0 AND n.grad = 'beograd'", Integer.class).getSingleResult();
        if (godinaIzgradnje == null) {
            return 0;
        }
        return godinaIzgradnje.doubleValue();
    }

    public static double dohvatiNajveciBrojSoba() {
        BigDecimal brojSoba = em.createQuery("SELECT MAX(n.brojSoba) FROM Nekretnina n WHERE n.tip = 0 AND n.tipPonude = 0 AND n.grad = 'beograd'", BigDecimal.class).getSingleResult();
        if (brojSoba == null) {
            return 0;
        }
        return brojSoba.doubleValue();
    }

    public static double dohvatiNajvecuCenu() {
        Integer cena = em.createQuery("SELECT MAX(n.cena) FROM Nekretnina n WHERE n.tip = 0 AND n.tipPonude = 0 AND n.grad = 'beograd'", Integer.class).getSingleResult();
        if (cena == null) {
            return 0;
        }
        return cena.doubleValue();
    }

    public static String checkLength(String s, int length) {
        if (s == null) {
            return null;
        }
        String temp = s.toLowerCase().replaceAll("č", "c").replaceAll("ć", "c").replaceAll("ž", "z").replaceAll("š", "s").replaceAll("đ", "dj");
        return (temp.length() > length) ? temp.substring(0, length) : temp;
    }

    public static String checkLengthWithNull(String s, int length) {
        return (s != null && s.length() > length) ? null : s;
    }

    public static void obrisiNekretninu(Nekretnina nekretnina) {
        em.getTransaction().begin();
        em.remove(nekretnina);
        em.getTransaction().commit();
    }

    public static void promeniIdNekretnine(Nekretnina nekretnina, Integer id) {
        em.getTransaction().begin();
        nekretnina.setId(id);
        em.getTransaction().commit();
        em.refresh(nekretnina);
    }

    public static boolean daLiPostojiNekretninaSaLinkom(String link) {
        List<Nekretnina> nekretnine = em.createQuery("SELECT n FROM Nekretnina n WHERE n.link = :link", Nekretnina.class).setParameter("link", link).getResultList();
        return !(nekretnine == null || nekretnine.isEmpty());
    }

}
