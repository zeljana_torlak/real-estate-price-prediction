package nekretnine;

import java.util.List;
import java.util.Random;
import java.util.Scanner;
import nekretnine.entiteti.Nekretnina;

public class LinearnaRegresija {

    public enum Odlika {
        NULTA, KVADRATURA, GODINA_IZGRADNJE, UKNJIZENOST, BROJ_SOBA, PARKING, GARAZA, LIFT
    }

    private static final int BROJ_ODLIKA = Odlika.values().length; //+1 (NULTA)

    private static int BROJ_PODATAKA;
    private static int BROJ_PODATAKA_ZA_OBUCAVANJE;
    private static int BROJ_PODATAKA_ZA_TESTIRANJE;
    private static double[] xmax;
    private static double ymax;
    private static double[] w;
    private static double[][] xs;
    private static double[] y;

    private static final Random RANDOM = new Random();

    private static double skalirajOdliku(int index, double vrednost) {
        return vrednost / xmax[index];
    }

    private static double izracunajHipotezu(int index) {
        double sum = 0;
        for (int i = 0; i < BROJ_ODLIKA; i++) {
            sum += (w[i] * xs[index][i]);
        }
        return sum;
    }

    private static double izracunajHipotezu(double[] x) {
        double sum = 0;
        for (int i = 0; i < BROJ_ODLIKA; i++) {
            sum += (w[i] * x[i]);
        }
        return sum;
    }

    private static void inicijalizuj() {
        xmax = new double[BROJ_ODLIKA];
        xmax[Odlika.NULTA.ordinal()] = 1;
        xmax[Odlika.KVADRATURA.ordinal()] = DatabaseOperations.dohvatiNajvecuKvadraturu();
        xmax[Odlika.GODINA_IZGRADNJE.ordinal()] = DatabaseOperations.dohvatiNajvecuGodinuIzgradnje();
        xmax[Odlika.UKNJIZENOST.ordinal()] = 1;
        xmax[Odlika.BROJ_SOBA.ordinal()] = DatabaseOperations.dohvatiNajveciBrojSoba();
        xmax[Odlika.PARKING.ordinal()] = 1;
        xmax[Odlika.GARAZA.ordinal()] = 1;
        xmax[Odlika.LIFT.ordinal()] = 1;
        ymax = DatabaseOperations.dohvatiNajvecuCenu();

        w = new double[BROJ_ODLIKA];
        for (int i = 0; i < BROJ_ODLIKA; i++) {
            w[i] = 0;
        }

        List<Nekretnina> nekretnine = DatabaseOperations.dovatiSveStanoveKojiSeProdajuUBeogradu();
        BROJ_PODATAKA = nekretnine.size();
        xs = new double[BROJ_PODATAKA][BROJ_ODLIKA];
        y = new double[BROJ_PODATAKA];
        BROJ_PODATAKA_ZA_OBUCAVANJE = 0;
        BROJ_PODATAKA_ZA_TESTIRANJE = 0;

        int indeksZaObucavanje = 0;
        int indeksZaTestiranje = BROJ_PODATAKA - 1;
        while (!nekretnine.isEmpty()) {
            Nekretnina n = nekretnine.get(0);
            int indeks;
            if (RANDOM.nextInt(10) < 3) {
                indeks = indeksZaTestiranje;
                indeksZaTestiranje--;
                BROJ_PODATAKA_ZA_TESTIRANJE++;
            } else {
                indeks = indeksZaObucavanje;
                indeksZaObucavanje++;
                BROJ_PODATAKA_ZA_OBUCAVANJE++;
            }
            xs[indeks][Odlika.NULTA.ordinal()] = 1;
            xs[indeks][Odlika.KVADRATURA.ordinal()] = skalirajOdliku(Odlika.KVADRATURA.ordinal(), (n.getKvadratura() != null) ? n.getKvadratura().doubleValue() : 0); //ako je null procitan?
            xs[indeks][Odlika.GODINA_IZGRADNJE.ordinal()] = skalirajOdliku(Odlika.GODINA_IZGRADNJE.ordinal(), (n.getGodinaIzgradnje() != null) ? n.getGodinaIzgradnje().doubleValue() : 0);
            xs[indeks][Odlika.UKNJIZENOST.ordinal()] = skalirajOdliku(Odlika.UKNJIZENOST.ordinal(), (n.getUknjizenost() != null) ? n.getUknjizenost().doubleValue() : 0);
            xs[indeks][Odlika.BROJ_SOBA.ordinal()] = skalirajOdliku(Odlika.BROJ_SOBA.ordinal(), (n.getBrojSoba() != null) ? n.getBrojSoba().doubleValue() : 0);
            xs[indeks][Odlika.PARKING.ordinal()] = skalirajOdliku(Odlika.PARKING.ordinal(), (n.getParking() != null) ? n.getParking().doubleValue() : 0);
            xs[indeks][Odlika.GARAZA.ordinal()] = skalirajOdliku(Odlika.GARAZA.ordinal(), (n.getGaraza() != null) ? n.getGaraza().doubleValue() : 0);
            xs[indeks][Odlika.LIFT.ordinal()] = skalirajOdliku(Odlika.LIFT.ordinal(), (n.getLift() != null) ? n.getLift().doubleValue() : 0);

            y[indeks] = n.getCena() / ymax;

            nekretnine.remove(0);
        }

        System.out.println("BROJ_PODATAKA_ZA_OBUCAVANJE + BROJ_PODATAKA_ZA_TESTIRANJE = " + BROJ_PODATAKA_ZA_OBUCAVANJE + " + " + BROJ_PODATAKA_ZA_TESTIRANJE + " = " + (BROJ_PODATAKA_ZA_OBUCAVANJE + BROJ_PODATAKA_ZA_TESTIRANJE) + " = " + BROJ_PODATAKA);
    }

    private static double izracunajFunkcijuGreske(double lambda) {
        double sum1 = 0;
        for (int i = 0; i < BROJ_PODATAKA_ZA_OBUCAVANJE; i++) {
            sum1 += Math.pow(izracunajHipotezu(i) - y[i], 2);
        }
        double sum2 = 0;
        for (int i = 1; i < BROJ_ODLIKA; i++) {
            sum2 += Math.pow(w[i], 2);
        }
        sum2 *= lambda;
        return (sum1 + sum2) / (BROJ_PODATAKA_ZA_OBUCAVANJE * 2.0);
    }

    private static double izracunajFunkcijuGreskeNadPodacimaZaTestiranje(double lambda) {
        double sum1 = 0;
        for (int i = BROJ_PODATAKA - BROJ_PODATAKA_ZA_TESTIRANJE; i < BROJ_PODATAKA; i++) {
            sum1 += Math.pow(izracunajHipotezu(i) - y[i], 2);
        }
        double sum2 = 0;
        for (int i = 1; i < BROJ_ODLIKA; i++) {
            sum2 += Math.pow(w[i], 2);
        }
        sum2 *= lambda;
        return (sum1 + sum2) / (BROJ_PODATAKA_ZA_TESTIRANJE * 2.0);
    }

    private static double azuriranjeTezinskogParametra(double alfa, double lambda, int indexOdlike) {
        double sum = 0;
        for (int i = 0; i < BROJ_PODATAKA_ZA_OBUCAVANJE; i++) {
            sum += ((izracunajHipotezu(i) - y[i]) * xs[i][indexOdlike]);
        }
        sum += (lambda * w[indexOdlike]);
        sum *= alfa / (BROJ_PODATAKA_ZA_OBUCAVANJE * 1.0);
        return w[indexOdlike] - sum;
    }

    private static void grupniGradijentniSpust(double alfa, double lambda) {
        double[] wnew = new double[BROJ_ODLIKA];
        for (int i = 0; i < BROJ_ODLIKA; i++) {
            wnew[i] = azuriranjeTezinskogParametra(alfa, lambda, i);
        }
        for (int i = 0; i < BROJ_ODLIKA; i++) {
            w[i] = wnew[i];
        }
    }

    public static void analiziraj(double alfa, double lambda) { //koriscena grebena regresija
        inicijalizuj();

        double funkcijaGreske = izracunajFunkcijuGreske(lambda);
        int brojIteracije = 0;
        while (true) {
            System.out.println("Broj iteracije: " + brojIteracije + "; funkcija greske: " + funkcijaGreske);
            grupniGradijentniSpust(alfa, lambda);
            double novaFunkcijaGreske = izracunajFunkcijuGreske(lambda);
            if (Math.abs(funkcijaGreske - novaFunkcijaGreske) < 0.00001 * funkcijaGreske) {
                break;
            }
            funkcijaGreske = novaFunkcijaGreske;
            brojIteracije++;
        }

        System.out.println("Funkcija greske nad podacima za testiranje (" + BROJ_PODATAKA_ZA_TESTIRANJE + ") je: " + izracunajFunkcijuGreskeNadPodacimaZaTestiranje(lambda));
        //System.out.println("" + w[0] + "\t" + w[1] + "\t" + w[2] + "\t" + w[3] + "\t" + w[4] + "\t" + w[5] + "\t" + w[6]);
    }

    public static void ucitajPodatke() { //neophodno je da metoda analiziraj(alfa, lambda) bude pozvana pre
        double[] x = new double[BROJ_ODLIKA];
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Da li zelite da proverite prediktovanu cenu stana na osnovu linearne regresije? [y/n]");
            if (scanner.nextLine().equalsIgnoreCase("n")) {
                break;
            }

            System.out.println("Molimo Vas da unesete sledece podatke:");
            for (Odlika o : Odlika.values()) {
                if (o == Odlika.NULTA) {
                    x[Odlika.NULTA.ordinal()] = 1;
                    continue;
                }
                System.out.println(o.toString() + ": ");
                String temp = scanner.nextLine();
                if (!temp.isEmpty() && temp.matches("(0|[1-9]\\d*)")) {
                    x[o.ordinal()] = skalirajOdliku(o.ordinal(), Integer.parseInt(temp));
                } else if (!temp.isEmpty() && temp.matches("(0|[1-9]\\d*\\.\\d*)")) {
                    x[o.ordinal()] = skalirajOdliku(o.ordinal(), Double.parseDouble(temp));
                } else { //ako je null procitan?
                    x[o.ordinal()] = 0;
                }
            }

            System.out.println("Prediktovana vrednost cene: " + (izracunajHipotezu(x) * ymax) + "e");
        }
    }
}
