/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nekretnine.entiteti;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hp-650g1
 */
@Entity
@Table(name = "nekretnina")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Nekretnina.findAll", query = "SELECT n FROM Nekretnina n"),
    @NamedQuery(name = "Nekretnina.findById", query = "SELECT n FROM Nekretnina n WHERE n.id = :id"),
    @NamedQuery(name = "Nekretnina.findByTip", query = "SELECT n FROM Nekretnina n WHERE n.tip = :tip"),
    @NamedQuery(name = "Nekretnina.findByTipPonude", query = "SELECT n FROM Nekretnina n WHERE n.tipPonude = :tipPonude"),
    @NamedQuery(name = "Nekretnina.findByGrad", query = "SELECT n FROM Nekretnina n WHERE n.grad = :grad"),
    @NamedQuery(name = "Nekretnina.findByDeoGrada", query = "SELECT n FROM Nekretnina n WHERE n.deoGrada = :deoGrada"),
    @NamedQuery(name = "Nekretnina.findByKvadratura", query = "SELECT n FROM Nekretnina n WHERE n.kvadratura = :kvadratura"),
    @NamedQuery(name = "Nekretnina.findByGodinaIzgradnje", query = "SELECT n FROM Nekretnina n WHERE n.godinaIzgradnje = :godinaIzgradnje"),
    @NamedQuery(name = "Nekretnina.findByPovrsinaZemljista", query = "SELECT n FROM Nekretnina n WHERE n.povrsinaZemljista = :povrsinaZemljista"),
    @NamedQuery(name = "Nekretnina.findBySpratnost", query = "SELECT n FROM Nekretnina n WHERE n.spratnost = :spratnost"),
    @NamedQuery(name = "Nekretnina.findByUknjizenost", query = "SELECT n FROM Nekretnina n WHERE n.uknjizenost = :uknjizenost"),
    @NamedQuery(name = "Nekretnina.findByTipGrejanja", query = "SELECT n FROM Nekretnina n WHERE n.tipGrejanja = :tipGrejanja"),
    @NamedQuery(name = "Nekretnina.findByBrojSoba", query = "SELECT n FROM Nekretnina n WHERE n.brojSoba = :brojSoba"),
    @NamedQuery(name = "Nekretnina.findByUnutrasnjeProstorije", query = "SELECT n FROM Nekretnina n WHERE n.unutrasnjeProstorije = :unutrasnjeProstorije"),
    @NamedQuery(name = "Nekretnina.findByParking", query = "SELECT n FROM Nekretnina n WHERE n.parking = :parking"),
    @NamedQuery(name = "Nekretnina.findByGaraza", query = "SELECT n FROM Nekretnina n WHERE n.garaza = :garaza"),
    @NamedQuery(name = "Nekretnina.findByLift", query = "SELECT n FROM Nekretnina n WHERE n.lift = :lift"),
    @NamedQuery(name = "Nekretnina.findByInfrastruktura", query = "SELECT n FROM Nekretnina n WHERE n.infrastruktura = :infrastruktura"),
    @NamedQuery(name = "Nekretnina.findByCena", query = "SELECT n FROM Nekretnina n WHERE n.cena = :cena"),
    @NamedQuery(name = "Nekretnina.findByLink", query = "SELECT n FROM Nekretnina n WHERE n.link = :link")})
public class Nekretnina implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "tip")
    private Integer tip;
    @Column(name = "tipPonude")
    private Integer tipPonude;
    @Column(name = "grad")
    private String grad;
    @Column(name = "deoGrada")
    private String deoGrada;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "kvadratura")
    private BigDecimal kvadratura;
    @Column(name = "godinaIzgradnje")
    private Integer godinaIzgradnje;
    @Column(name = "povrsinaZemljista")
    private BigDecimal povrsinaZemljista;
    @Column(name = "spratnost")
    private String spratnost;
    @Column(name = "uknjizenost")
    private Integer uknjizenost;
    @Column(name = "tipGrejanja")
    private String tipGrejanja;
    @Column(name = "brojSoba")
    private BigDecimal brojSoba;
    @Column(name = "unutrasnjeProstorije")
    private String unutrasnjeProstorije;
    @Column(name = "parking")
    private Integer parking;
    @Column(name = "garaza")
    private Integer garaza;
    @Column(name = "lift")
    private Integer lift;
    @Column(name = "infrastruktura")
    private String infrastruktura;
    @Column(name = "cena")
    private Integer cena;
    @Column(name = "link")
    private String link;

    public Nekretnina() {
    }

    public Nekretnina(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTip() {
        return tip;
    }

    public void setTip(Integer tip) {
        this.tip = tip;
    }

    public Integer getTipPonude() {
        return tipPonude;
    }

    public void setTipPonude(Integer tipPonude) {
        this.tipPonude = tipPonude;
    }

    public String getGrad() {
        return grad;
    }

    public void setGrad(String grad) {
        this.grad = grad;
    }

    public String getDeoGrada() {
        return deoGrada;
    }

    public void setDeoGrada(String deoGrada) {
        this.deoGrada = deoGrada;
    }

    public BigDecimal getKvadratura() {
        return kvadratura;
    }

    public void setKvadratura(BigDecimal kvadratura) {
        this.kvadratura = kvadratura;
    }

    public Integer getGodinaIzgradnje() {
        return godinaIzgradnje;
    }

    public void setGodinaIzgradnje(Integer godinaIzgradnje) {
        this.godinaIzgradnje = godinaIzgradnje;
    }

    public BigDecimal getPovrsinaZemljista() {
        return povrsinaZemljista;
    }

    public void setPovrsinaZemljista(BigDecimal povrsinaZemljista) {
        this.povrsinaZemljista = povrsinaZemljista;
    }

    public String getSpratnost() {
        return spratnost;
    }

    public void setSpratnost(String spratnost) {
        this.spratnost = spratnost;
    }

    public Integer getUknjizenost() {
        return uknjizenost;
    }

    public void setUknjizenost(Integer uknjizenost) {
        this.uknjizenost = uknjizenost;
    }

    public String getTipGrejanja() {
        return tipGrejanja;
    }

    public void setTipGrejanja(String tipGrejanja) {
        this.tipGrejanja = tipGrejanja;
    }

    public BigDecimal getBrojSoba() {
        return brojSoba;
    }

    public void setBrojSoba(BigDecimal brojSoba) {
        this.brojSoba = brojSoba;
    }

    public String getUnutrasnjeProstorije() {
        return unutrasnjeProstorije;
    }

    public void setUnutrasnjeProstorije(String unutrasnjeProstorije) {
        this.unutrasnjeProstorije = unutrasnjeProstorije;
    }

    public Integer getParking() {
        return parking;
    }

    public void setParking(Integer parking) {
        this.parking = parking;
    }

    public Integer getGaraza() {
        return garaza;
    }

    public void setGaraza(Integer garaza) {
        this.garaza = garaza;
    }

    public Integer getLift() {
        return lift;
    }

    public void setLift(Integer lift) {
        this.lift = lift;
    }

    public String getInfrastruktura() {
        return infrastruktura;
    }

    public void setInfrastruktura(String infrastruktura) {
        this.infrastruktura = infrastruktura;
    }

    public Integer getCena() {
        return cena;
    }

    public void setCena(Integer cena) {
        this.cena = cena;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nekretnina)) {
            return false;
        }
        Nekretnina other = (Nekretnina) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "nekretnine.entiteti.Nekretnina[ id=" + id + " ]";
    }
    
}
