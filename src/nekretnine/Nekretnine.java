package nekretnine;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import nekretnine.entiteti.Nekretnina;

public class Nekretnine {

    public enum TipNekretnine {
        STAN, KUCA
    }

    public enum TipPonude {
        PRODAJA, IZNAJMLJIVANJE
    }

    public static void dohvatiOglase() {
        for (int i = 0; i < 50; i++) {
            WebCrawler.dohvatiOglase("https://www.4zida.rs/prodaja-stanova?skuplje_od=" + (i * 5000) + "eur&jeftinije_od=" + ((i + 1) * 5000) + "eur&sortiranje=najnoviji", TipNekretnine.STAN.ordinal(), TipPonude.PRODAJA.ordinal());
        }
        WebCrawler.dohvatiOglase("https://www.4zida.rs/prodaja-stanova?skuplje_od=250000eur&sortiranje=najnoviji", TipNekretnine.STAN.ordinal(), TipPonude.PRODAJA.ordinal());

        for (int i = 0; i < 20; i++) {
            WebCrawler.dohvatiOglase("https://www.4zida.rs/izdavanje-stanova?skuplje_od=" + (i * 50) + "eur&jeftinije_od=" + ((i + 1) * 50) + "eur&sortiranje=najnoviji", TipNekretnine.STAN.ordinal(), TipPonude.IZNAJMLJIVANJE.ordinal());
        }
        WebCrawler.dohvatiOglase("https://www.4zida.rs/izdavanje-stanova?skuplje_od=1000eur&sortiranje=najnoviji", TipNekretnine.STAN.ordinal(), TipPonude.IZNAJMLJIVANJE.ordinal());
        
        for (int i = 0; i < 25; i++) {
        WebCrawler.dohvatiOglase("https://www.4zida.rs/prodaja-kuca?skuplje_od=" + (i * 20000) + "eur&jeftinije_od=" + ((i + 1) * 20000) + "eur&sortiranje=najnoviji", TipNekretnine.KUCA.ordinal(), TipPonude.PRODAJA.ordinal());
        }
        WebCrawler.dohvatiOglase("https://www.4zida.rs/prodaja-kuca?skuplje_od=500000eur&sortiranje=najnoviji", TipNekretnine.KUCA.ordinal(), TipPonude.PRODAJA.ordinal());
        
        WebCrawler.dohvatiOglase("https://www.4zida.rs/izdavanje-kuca?sortiranje=najnoviji", TipNekretnine.KUCA.ordinal(), TipPonude.IZNAJMLJIVANJE.ordinal());
    }

    public static LinkedList<LinkedList<Nekretnina>> pronadjiDuplikate() {
        List<Nekretnina> nekretnine = DatabaseOperations.dovatiSveNekretnine();
        LinkedList<LinkedList<Nekretnina>> duplikati = new LinkedList<>();

        while (!nekretnine.isEmpty()) {
            Nekretnina nekretnina = nekretnine.get(0);
            LinkedList<Integer> indeksi = new LinkedList<>();

            for (int i = 1; i < nekretnine.size(); i++) {
                Nekretnina n = nekretnine.get(i);
                if (Objects.equals(n.getLink(), nekretnina.getLink())
                        && Objects.equals(n.getTip(), nekretnina.getTip())
                        && Objects.equals(n.getTipPonude(), nekretnina.getTipPonude())
                        && Objects.equals(n.getGrad(), nekretnina.getGrad())
                        && Objects.equals(n.getDeoGrada(), nekretnina.getDeoGrada())
                        && Objects.equals(n.getKvadratura(), nekretnina.getKvadratura())
                        && Objects.equals(n.getGodinaIzgradnje(), nekretnina.getGodinaIzgradnje())
                        && Objects.equals(n.getPovrsinaZemljista(), nekretnina.getPovrsinaZemljista())
                        && Objects.equals(n.getSpratnost(), nekretnina.getSpratnost())
                        && Objects.equals(n.getUknjizenost(), nekretnina.getUknjizenost())
                        && Objects.equals(n.getTipGrejanja(), nekretnina.getTipGrejanja())
                        && Objects.equals(n.getBrojSoba(), nekretnina.getBrojSoba())
                        && Objects.equals(n.getUnutrasnjeProstorije(), nekretnina.getUnutrasnjeProstorije())
                        && Objects.equals(n.getParking(), nekretnina.getParking())
                        && Objects.equals(n.getGaraza(), nekretnina.getGaraza())
                        && Objects.equals(n.getLift(), nekretnina.getLift())
                        && Objects.equals(n.getInfrastruktura(), nekretnina.getInfrastruktura())
                        && Objects.equals(n.getCena(), nekretnina.getCena())) {
                    indeksi.add(i);
                }
            }
            if (!indeksi.isEmpty()) {
                LinkedList<Nekretnina> d = new LinkedList<>();
                for (int i = indeksi.size() - 1; i >= 0; i--) {
                    d.add(nekretnine.get(indeksi.get(i)));
                    nekretnine.remove((int) indeksi.get(i));
                }
                d.add(nekretnina);
                duplikati.add(d);
            }

            nekretnine.remove(0);
        }

        return duplikati;
    }

    public static void ispisiDuplikate(LinkedList<LinkedList<Nekretnina>> duplikati) {
        int sum = 0;
        for (int i = 0; i < duplikati.size(); i++) {
            sum += duplikati.get(i).size();
            for (int j = 0; j < duplikati.get(i).size(); j++) {
                Nekretnina n = duplikati.get(i).get(j);
                System.out.println("" + n.getId() + "\t" + n.getTip() + "\t" + n.getTipPonude() + "\t" + n.getGrad() + "\t" + n.getDeoGrada() + "\t"
                        + n.getKvadratura() + "\t" + n.getGodinaIzgradnje() + "\t" + n.getPovrsinaZemljista() + "\t" + n.getSpratnost() + "\t" + n.getUknjizenost() + "\t"
                        + n.getTipGrejanja() + "\t" + n.getBrojSoba() + "\t" + n.getUnutrasnjeProstorije() + "\t" + n.getParking() + "\t" + n.getGaraza() + "\t"
                        + n.getLift() + "\t" + n.getInfrastruktura() + "\t" + n.getCena() + "\t" + n.getLink() + "\t");
            }
            System.out.println("------------");
        }
        System.out.println("Broj originala sa duplikatima: " + duplikati.size());
        System.out.println("Ukupno duplikata: " + (sum - duplikati.size()));
    }

    public static void obrisiDuplikate(LinkedList<LinkedList<Nekretnina>> duplikati) {
        for (int i = 0; i < duplikati.size(); i++) {
            for (int j = 0; j < (duplikati.get(i).size() - 1); j++) {
                DatabaseOperations.obrisiNekretninu(duplikati.get(i).get(j));
            }
        }
    }

    public static void ciscenjeBaze() {
        LinkedList<LinkedList<Nekretnina>> duplikati = pronadjiDuplikate();
        ispisiDuplikate(duplikati);
        //obrisiDuplikate(duplikati);
    }

    public static void setujIdentifikatore() { //moze da radi samo ako id nije primaran kljuc
        List<Nekretnina> nekretnine = DatabaseOperations.dovatiSveNekretnine();

        for (int i = 0; i < nekretnine.size(); i++) {
            Nekretnina nekretnina = nekretnine.get(i);
            if (nekretnina.getId() == (i + 1)) {
                continue;
            }
            DatabaseOperations.promeniIdNekretnine(nekretnina, i + 1);
        }

    }

    public static void linearnaRegresija() {
        LinearnaRegresija.analiziraj(0.75, 1.9);
        LinearnaRegresija.ucitajPodatke();
    }

    public static void main(String[] args) throws IOException {
        //dohvatiOglase();

        //ciscenjeBaze();
        //setujIdentifikatore();
        linearnaRegresija();

//        KNajblizihSuseda.analiziraj(KNajblizihSuseda.TipRazdaljine.EUKLIDSKA);
        KNajblizihSuseda.ucitajPodatke(KNajblizihSuseda.TipRazdaljine.EUKLIDSKA);
//        KNajblizihSuseda.analiziraj(KNajblizihSuseda.TipRazdaljine.MENHETN);
        KNajblizihSuseda.ucitajPodatke(KNajblizihSuseda.TipRazdaljine.MENHETN);
    }

}
