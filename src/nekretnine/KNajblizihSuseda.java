package nekretnine;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import nekretnine.entiteti.Nekretnina;

public class KNajblizihSuseda {

    public enum Odlika {
        KVADRATURA, GODINA_IZGRADNJE, UKNJIZENOST, BROJ_SOBA, PARKING, GARAZA, LIFT
    }

    public enum KlasaCene {
        LT50k, BT50AND100k, BT100AND150k, BT150AND200k, GT200k
    }

    public enum TipRazdaljine {
        EUKLIDSKA, MENHETN
    }

    private static final int BROJ_ODLIKA = Odlika.values().length;
    private static final int BROJ_KLASA = KlasaCene.values().length;

    private static int BROJ_PODATAKA;
    private static double[] xmax;
    private static double[][] xs;
    private static KlasaCene[] y;
    private static int k;

    private static double skalirajOdliku(int index, double vrednost) {
        return vrednost / xmax[index];
    }

    private static KlasaCene izracunajPrediktivnuKlasu(LinkedList<Double> udaljenosti) {
        Integer[] brojac = new Integer[BROJ_KLASA];
        Arrays.fill(brojac, 0);
        for (int i = 0; i < k; i++) {
            int minIndex = udaljenosti.indexOf(Collections.min(udaljenosti));
            brojac[y[minIndex].ordinal()]++;
            udaljenosti.set(minIndex, Double.MAX_VALUE);
        }

        List<Integer> listaBrojaca = Arrays.asList(brojac);
        return KlasaCene.values()[listaBrojaca.indexOf(Collections.max(listaBrojaca))];
    }

    private static KlasaCene izracunajHipotezu(int index, TipRazdaljine tipRazdaljine) {
        LinkedList<Double> udaljenosti = new LinkedList<>();
        for (int i = 0; i < BROJ_PODATAKA; i++) {
            if (i == index) {
                udaljenosti.add(Double.MAX_VALUE);
            } else if (tipRazdaljine == TipRazdaljine.EUKLIDSKA) {
                udaljenosti.add(izracunajEuklidskuRazdaljinu(xs[index], xs[i]));
            } else {
                udaljenosti.add(izracunajMenhetnRazdaljinu(xs[index], xs[i]));
            }
        }

        return izracunajPrediktivnuKlasu(udaljenosti);
    }

    private static KlasaCene izracunajHipotezu(double[] x, TipRazdaljine tipRazdaljine) {
        LinkedList<Double> udaljenosti = new LinkedList<>();
        for (int i = 0; i < BROJ_PODATAKA; i++) {
            if (tipRazdaljine == TipRazdaljine.EUKLIDSKA) {
                udaljenosti.add(izracunajEuklidskuRazdaljinu(x, xs[i]));
            } else {
                udaljenosti.add(izracunajMenhetnRazdaljinu(x, xs[i]));
            }
        }

        return izracunajPrediktivnuKlasu(udaljenosti);
    }

    private static KlasaCene odrediKlasuNaOsnovuCene(Integer cena) {
        if (cena < 50000) {
            return KlasaCene.LT50k;
        } else if (cena < 100000) {
            return KlasaCene.BT50AND100k;
        } else if (cena < 150000) {
            return KlasaCene.BT100AND150k;
        } else if (cena < 200000) {
            return KlasaCene.BT150AND200k;
        } else {
            return KlasaCene.GT200k;
        }
    }

    private static double izracunajEuklidskuRazdaljinu(double[] x1, double[] x2) { // Euclidean distance
        double sum = 0;
        for (int i = 0; i < BROJ_ODLIKA; i++) {
            sum += Math.pow(x1[i] - x2[i], 2);
        }
        return Math.sqrt(sum);
    }

    private static double izracunajMenhetnRazdaljinu(double[] x1, double[] x2) { // Manhattan distance, taxicab metric
        double sum = 0;
        for (int i = 0; i < BROJ_ODLIKA; i++) {
            sum += Math.abs(x1[i] - x2[i]);
        }
        return sum;
    }

    private static void inicijalizuj() {
        xmax = new double[BROJ_ODLIKA];
        xmax[Odlika.KVADRATURA.ordinal()] = DatabaseOperations.dohvatiNajvecuKvadraturu();
        xmax[Odlika.GODINA_IZGRADNJE.ordinal()] = DatabaseOperations.dohvatiNajvecuGodinuIzgradnje();
        xmax[Odlika.UKNJIZENOST.ordinal()] = 1;
        xmax[Odlika.BROJ_SOBA.ordinal()] = DatabaseOperations.dohvatiNajveciBrojSoba();
        xmax[Odlika.PARKING.ordinal()] = 1;
        xmax[Odlika.GARAZA.ordinal()] = 1;
        xmax[Odlika.LIFT.ordinal()] = 1;

        List<Nekretnina> nekretnine = DatabaseOperations.dovatiSveStanoveKojiSeProdajuUBeogradu();
        BROJ_PODATAKA = nekretnine.size();
        xs = new double[BROJ_PODATAKA][BROJ_ODLIKA];
        y = new KlasaCene[BROJ_PODATAKA];

        int index = 0;
        while (!nekretnine.isEmpty()) {
            Nekretnina n = nekretnine.get(0);
            xs[index][Odlika.KVADRATURA.ordinal()] = skalirajOdliku(Odlika.KVADRATURA.ordinal(), (n.getKvadratura() != null) ? n.getKvadratura().doubleValue() : 0); //ako je null procitan?
            xs[index][Odlika.GODINA_IZGRADNJE.ordinal()] = skalirajOdliku(Odlika.GODINA_IZGRADNJE.ordinal(), (n.getGodinaIzgradnje() != null) ? n.getGodinaIzgradnje().doubleValue() : 0);
            xs[index][Odlika.UKNJIZENOST.ordinal()] = skalirajOdliku(Odlika.UKNJIZENOST.ordinal(), (n.getUknjizenost() != null) ? n.getUknjizenost().doubleValue() : 0);
            xs[index][Odlika.BROJ_SOBA.ordinal()] = skalirajOdliku(Odlika.BROJ_SOBA.ordinal(), (n.getBrojSoba() != null) ? n.getBrojSoba().doubleValue() : 0);
            xs[index][Odlika.PARKING.ordinal()] = skalirajOdliku(Odlika.PARKING.ordinal(), (n.getParking() != null) ? n.getParking().doubleValue() : 0);
            xs[index][Odlika.GARAZA.ordinal()] = skalirajOdliku(Odlika.GARAZA.ordinal(), (n.getGaraza() != null) ? n.getGaraza().doubleValue() : 0);
            xs[index][Odlika.LIFT.ordinal()] = skalirajOdliku(Odlika.LIFT.ordinal(), (n.getLift() != null) ? n.getLift().doubleValue() : 0);

            y[index] = odrediKlasuNaOsnovuCene(n.getCena());

            index++;
            nekretnine.remove(0);
        }
    }

    private static void inicijalizujK() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Da li želite da unesete broj relevantnih najblizih suseda (y/n)");
        if (scanner.nextLine().toLowerCase().equals("y")) {
            System.out.println("Molimo Vas da unesete broj relevantnih najblizih suseda (parametar k):");
            String temp = scanner.nextLine();
            if (!temp.isEmpty() && temp.matches("(0|[1-9]\\d*)")) {
                k = Integer.parseInt(temp);
                if (k % 2 == 0) {
                    k++;
                }
                return;
            }
        }

        double tmp = Math.sqrt(BROJ_PODATAKA);
        tmp = Math.floor(tmp);
        if (((int) tmp) % 2 == 0) {
            tmp++;
        }
        k = (int) tmp;
    }

    public static void analiziraj(TipRazdaljine tipRazdaljine) {
        inicijalizuj();
        inicijalizujK();
        System.out.println("k = " + k);

        int brojTacnihPredikcija = 0;
        for (int i = 0; i < BROJ_PODATAKA; i++) {
            if (izracunajHipotezu(i, tipRazdaljine) == y[i]) {
                brojTacnihPredikcija++;
            }
        }
        System.out.println("Procenat uspesnosti predvidjanja je: " + brojTacnihPredikcija + " / " + BROJ_PODATAKA + " = " + (brojTacnihPredikcija * 100 / (BROJ_PODATAKA * 1.0)) + "% (tip razdaljine = " + tipRazdaljine.toString() + ")");
    }

    public static void ucitajPodatke(TipRazdaljine tipRazdaljine) {
        inicijalizuj();
        inicijalizujK();
        System.out.println("k = " + k);

        double[] x = new double[BROJ_ODLIKA];
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Da li zelite da proverite prediktovanu cenu stana na osnovu k najblizih suseda? (tip razdaljine = " + tipRazdaljine.toString() + ") [y/n]");
            if (scanner.nextLine().equalsIgnoreCase("n")) {
                break;
            }
            System.out.println("Molimo Vas da unesete sledece podatke:");
            for (Odlika o : Odlika.values()) {
                System.out.println(o.toString() + ": ");
                String temp = scanner.nextLine();
                if (!temp.isEmpty() && temp.matches("(0|[1-9]\\d*)")) {
                    x[o.ordinal()] = skalirajOdliku(o.ordinal(), Integer.parseInt(temp));
                } else if (!temp.isEmpty() && temp.matches("(0|[1-9]\\d*)\\.(\\d*)")) {
                    x[o.ordinal()] = skalirajOdliku(o.ordinal(), Double.parseDouble(temp));
                } else { //ako je null procitan?
                    x[o.ordinal()] = 0;
                }
            }

            System.out.println("Prediktovana vrednost cene: " + izracunajHipotezu(x, tipRazdaljine).toString());
        }
    }
}
