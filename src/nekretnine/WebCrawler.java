package nekretnine;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class WebCrawler {

    public static void dohvatiOglase(String linkDoPocetneStranice, int tipNekretnine, int tipPonude) {
        String url = linkDoPocetneStranice;
        Document doc;
        try {
            while (true) {
                System.out.println(url);
                if (url.contains("101")) { //kako bi se prekinulo parsiranje nakon 100te strane, jer tada pocinju dupli oglasi
                    break;
                }
                doc = Jsoup.connect(url).get();

                Elements oglasi = doc.select(".ad-preview-card");//.featured
                for (Element oglas : oglasi) {
                    Element linkDoOglasa = oglas.selectFirst("a");
                    if (linkDoOglasa != null) {
                        dohvatiOglas(linkDoOglasa.attr("abs:href"), tipNekretnine, tipPonude);
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(WebCrawler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }

                Element linkDoNaredneStranice = doc.selectFirst(".page-link.fa.fa-angle-double-right.arrow");
                if (linkDoNaredneStranice == null) {
                    break;
                }

                url = linkDoNaredneStranice.attr("abs:href"); // linkDoNaredneStranice.attr("href") za relativnu putanju

                try {
                    Thread.sleep(2500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(WebCrawler.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        } catch (IOException ex) {
            Logger.getLogger(WebCrawler.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println(url);
        } catch (Exception ex) {
            Logger.getLogger(WebCrawler.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println(url);
        }

    }

    private static void dohvatiOglas(String url, int tipNekretnine, int tipPonudeNekretnine) {
        if (DatabaseOperations.daLiPostojiNekretninaSaLinkom(url)) {
            return;
        }

        try {
            Document doc = Jsoup.connect(url).get();

            Integer tip = tipNekretnine;
            Integer tipPonude = tipPonudeNekretnine;

            LinkedList<String> lokacija = dohvatiLokaciju(doc);
            String grad = lokacija.get(0);
            String deoGrada = lokacija.get(1);

            BigDecimal kvadratura = dohvatiKvadraturu(doc);
            Integer godinaIzgradnje = dohvatiGodinuIzgradnje(doc);
            BigDecimal povrsinaZemljista = dohvatiPovrsinuZemljista(doc, tipNekretnine);

            String spratnost = dohvatiPolje(doc, "Spratnost");
            Integer uknjizenost = dohvatiUknjizenost(doc);
            String tipGrejanja = dohvatiPolje(doc, "Grejanje");
            BigDecimal brojSoba = dohvatiBrojSoba(doc);
            String unutrasnjeProstorije = dohvatiPolje(doc, "Unutrašnje prostorije");

            Integer parking = dohvatiParking(doc);
            Integer garaza = dohvatiGarazu(doc);
            Integer lift = dohvatiLift(doc);
            String infrastruktura = dohvatiPolje(doc, "Infrastruktura");
            Integer cena = dohvatiCenu(doc);

//            System.out.println("Grad : " + grad);
//            System.out.println("Deo grada : " + deoGrada);
//            System.out.println("Kvadratura : " + kvadratura);
//            System.out.println("Godina izgradnje : " + godinaIzgradnje);
//            System.out.println("Povrsina zemljista : " + povrsinaZemljista);
//            System.out.println("Spratnost : " + spratnost);
//            System.out.println("Uknjizenost : " + uknjizenost);
//            System.out.println("Tip grejanja : " + tipGrejanja);
//            System.out.println("Broj soba : " + brojSoba);
//            System.out.println("Unutrasnje prostorije : " + unutrasnjeProstorije);
//            System.out.println("Parking : " + parking);
//            System.out.println("Garaza : " + garaza);
//            System.out.println("Lift : " + lift);
//            System.out.println("Infrastruktura : " + infrastruktura);
//            System.out.println("Cena : " + cena);
//            System.out.println("");
            DatabaseOperations.napraviNekretninu(tip, tipPonude, grad,
                    deoGrada, kvadratura, godinaIzgradnje,
                    povrsinaZemljista, spratnost, uknjizenost,
                    tipGrejanja, brojSoba, unutrasnjeProstorije,
                    parking, garaza, lift, infrastruktura, cena, url);
        } catch (IOException ex) {
            Logger.getLogger(WebCrawler.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println(url);
        } catch (Exception ex) {
            Logger.getLogger(WebCrawler.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println(url);
        }
    }

    private static LinkedList<String> dohvatiLokaciju(Document doc) {
        Element lokacijaPolje = doc.selectFirst(".location");
        if (lokacijaPolje != null) {
            String lokacija = lokacijaPolje.text()
                    .replace(", Gradske lokacije", "").replace(", gradske lokacije", "").replace(",Gradske lokacije", "").replace(",gradske lokacije", "");

            if (lokacija != null) {
                if (lokacija.contains(",")) {
                    String grad = lokacija.substring(lokacija.lastIndexOf(",") + 1);
                    if (grad != null && grad.charAt(0) == ' ') {
                        grad = grad.substring(1);
                    }
                    String deoGrada = lokacija.substring(0, lokacija.lastIndexOf(","));

                    return new LinkedList<>(Arrays.asList(grad, deoGrada));
                } else {
                    return new LinkedList<>(Arrays.asList(lokacija, lokacija));
                }
            }
        }
        return new LinkedList<>(Arrays.asList(null, null));
    }

    private static BigDecimal dohvatiKvadraturu(Document doc) {
        String kvadratura = dohvatiPolje(doc, "Površina");
        if (kvadratura != null && kvadratura.split("[^0-9]+").length > 0) {
            String[] kvadraturaNiz = kvadratura.split("[^0-9]+");
            if (kvadraturaNiz.length > 1
                    && (kvadratura.contains(kvadraturaNiz[0] + "." + kvadraturaNiz[1]) || kvadratura.contains(kvadraturaNiz[0] + "," + kvadraturaNiz[1]))) {
                return new BigDecimal(kvadraturaNiz[0] + "." + kvadraturaNiz[1]);
            }
            return new BigDecimal(kvadraturaNiz[0]);
        }
        return null;
    }

    private static Integer dohvatiGodinuIzgradnje(Document doc) {
        String godinaIzgradnje = dohvatiPolje(doc, "Godina izgradnje");
        if (godinaIzgradnje != null && godinaIzgradnje.split("[^0-9]+").length > 0) {
            godinaIzgradnje = godinaIzgradnje.split("[^0-9]+")[0];
            if (godinaIzgradnje.length() != 4) {
                return null;
            }
            return Integer.parseInt(godinaIzgradnje);
        }
        return null;
    }

    private static BigDecimal dohvatiPovrsinuZemljista(Document doc, int tipNekretnine) {
        if (tipNekretnine == Nekretnine.TipNekretnine.KUCA.ordinal()) {
            String plac = dohvatiPolje(doc, "Plac");
            if (plac != null && plac.split("[^0-9]+").length > 0) {
                String[] placNiz = plac.split("[^0-9]+");
                if (placNiz.length > 1
                        && (plac.contains(placNiz[0] + "." + placNiz[1]) || plac.contains(placNiz[0] + "," + placNiz[1]))) {
                    return new BigDecimal(placNiz[0] + "." + placNiz[1]);
                }
                return new BigDecimal(placNiz[0]);
            }
        }
        return null;
    }

    private static Integer dohvatiUknjizenost(Document doc) {
        String uknjizenost = dohvatiPolje(doc, "Uknjiženost");
        if (uknjizenost != null) {
            if (uknjizenost.contains("uknjiženo") || uknjizenost.contains("uknjizeno")) {
                return 1;
            }
            return 0;
        }
        return null;
    }

    private static BigDecimal dohvatiBrojSoba(Document doc) {
        String brojSoba = dohvatiPolje(doc, "Broj soba");
        if (brojSoba != null && brojSoba.split("[^0-9]+").length > 0) {
            String[] brojSobaNiz = brojSoba.split("[^0-9]+");
            if (brojSobaNiz.length > 1
                    && (brojSoba.contains(brojSobaNiz[0] + "." + brojSobaNiz[1]) || brojSoba.contains(brojSobaNiz[0] + "," + brojSobaNiz[1]))) {
                return new BigDecimal(brojSobaNiz[0] + "." + brojSobaNiz[1]);
            }
            return new BigDecimal(brojSobaNiz[0]);
        }
        return null;
    }

    private static Integer dohvatiParking(Document doc) {
        String parking = dohvatiPolje(doc, "Parking");
        if (parking != null) {
            if (parking.contains("ima") || parking.split("[^0-9]+").length > 0) {
                return 1;
            }
            return 0;
        }
        return null;
    }

    private static Integer dohvatiGarazu(Document doc) {
        String garaza = dohvatiPolje(doc, "Garaža");
        if (garaza != null) {
            if (garaza.contains("ima") || garaza.split("[^0-9]+").length > 0) {
                return 1;
            }
            return 0;
        }
        return null;
    }

    private static Integer dohvatiLift(Document doc) {
        String lift = dohvatiPolje(doc, "Lift");
        if (lift != null) {
            if (lift.contains("ima") || lift.split("[^0-9]+").length > 0) {
                return 1;
            }
            return 0;
        }
        return null;
    }

    private static Integer dohvatiCenu(Document doc) {
        Element cenaPolje = doc.selectFirst(".prices.ng-star-inserted");
        if (cenaPolje != null) {
            String cena = cenaPolje.text();
            if (cena != null && cena.split("[^0-9]+").length > 0) {
                cena = cena.replaceAll("\\.", "");
                cena = cena.split("[^0-9]+")[0];
                return Integer.parseInt(cena);
            }
        }
        return null;
    }

    private static String dohvatiPolje(Document doc, String imePolja) {
        Element polje = doc.selectFirst("[label='" + imePolja + "']");
        if (polje != null) {
            Element vrednost = polje.selectFirst(".value");
            if (vrednost != null) {
                return vrednost.text();
            }
        }
        return null;
    }

}
