SELECT COUNT(*) FROM nekretnine.nekretnina;

//a
SELECT N.deoGrada, COUNT(*) AS 'Broj ponuda'
FROM nekretnine.nekretnina N
WHERE N.grad = "beograd"
GROUP BY N.deoGrada
ORDER BY COUNT(*) DESC
LIMIT 10;

//b
SELECT COUNT(IF(N.kvadratura <= 35,1,null)) AS '<35',
	COUNT(IF(N.kvadratura > 35 AND N.kvadratura <= 50,1,null)) AS '36-50',
	COUNT(IF(N.kvadratura > 50 AND N.kvadratura <= 65,1,null)) AS '51-65',
	COUNT(IF(N.kvadratura > 65 AND N.kvadratura <= 80,1,null)) AS '66-80',
	COUNT(IF(N.kvadratura > 80 AND N.kvadratura <= 95,1,null)) AS '81-95',
	COUNT(IF(N.kvadratura > 95 AND N.kvadratura <= 110,1,null)) AS '96-110',
	COUNT(IF(N.kvadratura > 110,1,null)) AS '>111'
FROM nekretnine.nekretnina N
WHERE N.tip = 0 AND N.tipPonude = 0;

//c
SELECT COUNT(IF(N.godinaIzgradnje <= 1950,1,null)) AS '<=1950',
	COUNT(IF(N.godinaIzgradnje >= 1951 AND N.godinaIzgradnje <= 1960,1,null)) AS '1951-1960',
	COUNT(IF(N.godinaIzgradnje >= 1961 AND N.godinaIzgradnje <= 1970,1,null)) AS '1961-1970',
	COUNT(IF(N.godinaIzgradnje >= 1971 AND N.godinaIzgradnje <= 1980,1,null)) AS '1971-1980',
	COUNT(IF(N.godinaIzgradnje >= 1981 AND N.godinaIzgradnje <= 1990,1,null)) AS '1981-1990',
	COUNT(IF(N.godinaIzgradnje >= 1991 AND N.godinaIzgradnje <= 2000,1,null)) AS '1991-2000',
	COUNT(IF(N.godinaIzgradnje >= 2001 AND N.godinaIzgradnje <= 2010,1,null)) AS '2001-2010',
	COUNT(IF(N.godinaIzgradnje >= 2011 AND N.godinaIzgradnje <= 2020,1,null)) AS '2011-2020',
	COUNT(IF(N.godinaIzgradnje >= 2021,1,null)) AS '>=2021'
FROM nekretnine.nekretnina N;

//d
SELECT N.grad, COUNT(*) AS 'Broj ponuda', 
	COUNT(IF(N.tipPonude=0,1,null)) AS 'Broj prodaja',
	COUNT(IF(N.tipPonude=1,1,null)) AS 'Broj iznajmljivanja'
FROM nekretnine.nekretnina N
GROUP BY N.grad
ORDER BY COUNT(*) DESC
LIMIT 5;

//e
SELECT COUNT(IF(N.cena <= 49999,1,null)) AS '<=49999',
	COUNT(IF(N.cena >= 50000 AND N.cena <= 99999,1,null)) AS '50000-99999',
	COUNT(IF(N.cena >= 100000 AND N.cena <= 149999,1,null)) AS '100000-149999',
	COUNT(IF(N.cena >= 150000 AND N.cena <= 199999,1,null)) AS '150000-199999',
	COUNT(IF(N.cena >= 200000,1,null)) AS '>=200000'
FROM nekretnine.nekretnina N
WHERE N.tipPonude = 0;

//f
SELECT COUNT(IF(N.parking = 1, 1, null)) AS 'Sa parkingom', COUNT(*) AS 'Ukupno'
FROM nekretnine.nekretnina N
WHERE N.tipPonude = 0 AND N.grad = 'beograd';

