SELECT COUNT(*) FROM nekretnine.nekretnina;

//a
//SELECT COUNT(*) AS 'Broj prodaja' FROM nekretnine.nekretnina N WHERE N.tipPonude = 0;
//SELECT COUNT(*) AS 'Broj iznajmljivanja' FROM nekretnine.nekretnina N WHERE N.tipPonude = 1;

SELECT COUNT(IF(N.tipPonude=0,1,null)) AS 'Broj prodaja',
	COUNT(IF(N.tipPonude=1,1,null)) AS 'Broj iznajmljivanja'
FROM nekretnine.nekretnina N;

//b
SELECT N.grad, COUNT(*) AS 'Broj prodaja'
FROM nekretnine.nekretnina N
WHERE N.tipPonude = 0
GROUP BY N.grad;

//c
SELECT COUNT(IF(N.tip=1 AND N.uknjizenost=1,1,null)) AS 'Broj uknjizenih kuca',
	COUNT(IF(N.tip=1 AND N.uknjizenost=0,1,null)) AS 'Broj neuknjizenih kuca',
	COUNT(IF(N.tip=0 AND N.uknjizenost=1,1,null)) AS 'Broj uknjizenih stanova',
	COUNT(IF(N.tip=0 AND N.uknjizenost=0,1,null)) AS 'Broj neuknjizenih stanova'
FROM nekretnine.nekretnina N;

//d
SELECT *
FROM nekretnine.nekretnina N
WHERE N.tip = 1 AND N.tipPonude = 0
ORDER BY N.cena DESC
LIMIT 30;

SELECT *
FROM nekretnine.nekretnina N
WHERE N.tip = 0 AND N.tipPonude = 0
ORDER BY N.cena DESC
LIMIT 30;

//e
SELECT *
FROM nekretnine.nekretnina N
WHERE N.tip = 1
ORDER BY N.kvadratura DESC
LIMIT 100;

SELECT *
FROM nekretnine.nekretnina N
WHERE N.tip = 0
ORDER BY N.kvadratura DESC
LIMIT 100;

//f
SELECT *
FROM nekretnine.nekretnina N
WHERE N.tipPonude = 0 AND N.godinaIzgradnje = 2020
ORDER BY N.cena DESC;

SELECT *
FROM nekretnine.nekretnina N
WHERE N.tipPonude = 1 AND N.godinaIzgradnje = 2020
ORDER BY N.cena DESC;

//g
SELECT *
FROM nekretnine.nekretnina N
ORDER BY N.brojSoba DESC
LIMIT 30;

SELECT *
FROM nekretnine.nekretnina N
WHERE N.tip = 0
ORDER BY N.kvadratura DESC
LIMIT 30;

SELECT *
FROM nekretnine.nekretnina N
WHERE N.tip = 1
ORDER BY N.povrsinaZemljista DESC
LIMIT 30;
